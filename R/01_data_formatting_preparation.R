# load-packages
library(readxl)
library(tidyverse)
library(janitor)
library(lubridate)

# TEAMS--------------
# crea ID y remueve toda la info que sobra----
teams <- read_excel("data/raw/Teams_completo_Andrea.xlsx", sheet = 3) %>% rename(attendee_first_name = attendee_fisrt_name)#1017

sai <- read_excel("data/raw/Teams Festival - Oso & Stephane (2).xlsx", sheet = "Personas eliminadas") %>% janitor::clean_names()#8

entra <- read_excel("data/raw/Teams Festival - Oso & Stephane (2).xlsx", sheet = "Personas nuevas") %>% janitor::clean_names()#33

really_entra <- entra[which(!entra$attendee_email %in% teams$attendee_email),]#1

sai$attendee_email %in% teams$attendee_email #solo se van 3

teams <- teams %>% filter(!attendee_email %in% sai$attendee_email)#1014

teams <- bind_rows(teams, really_entra) %>% 
  select(one_of(names(entra))) %>% 
  distinct()#1015
teams <- teams %>% mutate(ID = row_number())#1015

# anonymize----
push <- teams %>% 
  select(ID, starts_with("assigned"))
write_csv(push, "data/01_raw_teams.csv")

#write----
teams <- teams %>%
  mutate(across(contains("name"), stringr::str_to_title)) %>% 
  mutate(across(contains("name"), ~textclean::replace_non_ascii(.))) 

write_csv(teams, "data/02_raw_teams.csv")# DONT PUSH THAT ONE!!!

# SESSIONS----

sessions <- read_csv("data/raw/sesiones_ID_Ago25_ast.csv") %>% 
  janitor::clean_names()#457
sessions <- sessions %>% 
  select(session_title, new_session_title, schedule_track_optional,
         limit_capacity_optional, date, time_start, time_end_optional) %>% 
  distinct() #porque habia dos demolition man 9#456

# fix drag keynote----
  sessions[stringr::str_detect(sessions$session_title, "Drag"), c("session_title", "new_session_title") ]$new_session_title <- "Inspirational Keynote: Drag Queen 1"
  sessions[stringr::str_detect(sessions$session_title, "Drag"), c("session_title", "new_session_title") ]
# write----  
write_csv(sessions, "data/03_raw_sessions.csv")



#unique new_sessions ----
  
new_sessions <- sessions %>% 
  filter(session_title == new_session_title | new_session_title %in% c("Beach book club 12",            
                                                                       "Human Foosball Session 7",      
                                                                       "Renderize TCCC Future Session 2")) %>% 
  mutate(new_time_start = time_start) %>% 
  mutate(new_time_end = time_end_optional) %>% 
  select(new_session_title, 
         lands = schedule_track_optional, 
         limit_capacity = limit_capacity_optional,
         date,
         new_time_start, new_time_end) %>% 
  distinct() %>% 
    #formatea fechas
    mutate(across(.cols = contains("date"), .fns = function(x) lubridate::as_date(x, format = "%d/%m/%Y"))) %>% 
    mutate(session_start = paste(date, new_time_start)) %>% 
    mutate(session_end   = paste(date, new_time_end)) %>% 
    mutate(session_start = as_datetime(session_start, format = "%Y-%m-%d %H:%M:%S")) %>% 
    #LE QUITA UN MINUTO PARA SABER SI SE SOBRELAPA O NO
    mutate(session_end   = as_datetime(session_end, format = "%Y-%m-%d %H:%M:%S") - 1) %>% 
    #calcula los intervalos
    mutate(session_interval = interval(session_start, session_end)) %>% 
    mutate(session_duration = as.duration(session_interval)) %>% 
    mutate(session_ID = row_number())

write_csv(new_sessions, "data/04_new_sessions.csv")
  
# crea una funcion que chequea si los intervalos se sobrelapan----
  mat_list <- list()
check_conflict <- function(x) {
  intervals <- x$session_interval
  x$scheduling_conflict <- "ok"
  x$time_conflict <- NA
  matriz <- matrix(nrow = length(intervals), ncol = length(intervals))
  for (i in seq_along(intervals)) {
    for (j in setdiff(seq_along(intervals), i)) {
      matriz[i, j] <- int_overlaps(intervals[i], intervals[j])
    }
    if (any(matriz[i,] == TRUE, na.rm = TRUE)) {
      x$scheduling_conflict[i] <- "conflict"
      x$time_conflict[i] <- paste(sort(c(i, which(matriz[i,] == TRUE))), collapse = ";")
    }
    mat_list[[i]] <<- matriz
  }
  return(x)
}


###Dont run this again!!! #####
#session_conflicts <- check_conflict(new_sessions)

#session_conflicts_df <- session_conflicts[[1]] %>% select(-scheduling_conflict)
#session_conflicts_df$session_ID <- new_sessions$session_ID 
#write_csv(session_conflicts_df, "data/05_session_conflict.csv")
session_conflicts_df <- read_csv("data/05_session_conflict.csv")

#session_conflicts_mat <- session_conflicts[[2]]#perdí todas las matrices por genia

which_conflicts <- function(title = NULL, id = NULL) {
  if (!is.null(title)) {
  d <- session_conflicts_df %>% filter(new_session_title == title)
}
  if (!is.null(id)) {
  d <- session_conflicts_df %>% filter(session_ID == id)
}
  land <- d %>% select(lands)
  d <- d %>% select(time_conflict) %>% str_split(";", simplify = T) %>% as.vector() %>% as.numeric()
  e <- session_conflicts_df %>% select(session_ID, new_session_title)
  e <- e[d,]
  return(e)
}

  
#ahora son 316 actividades
sessions <- sessions %>% 
  select(session_title, new_session_title)
sessions <- left_join(sessions, session_conflicts_df)#456
#perfect
write_csv(sessions, "data/06_sessions.csv")

# REGISTRATIONS------
registrations <- read_csv("data/raw/session_registrations_final.csv") %>% 
  janitor::clean_names() %>% 
  mutate(across(contains("name"), stringr::str_to_title)) %>% 
  mutate(across(contains("name"), ~textclean::replace_non_ascii(.)))#4263
sum(!teams$attendee_email %in% unique(registrations$attendee_email))#faltan 252
filter(registrations, attendee_email %in% teams$attendee_email)#faltan 252#3764
registrations %>% filter(attendee_email == "emanjarres@coca-cola.com") %>% View()
length(unique(registrations$attendee_email))#880 inscritos originales en la tabla grande
# anonimyze data early----
teams <- teams %>%
  select(ID, attendee_email, starts_with("assigned"))
registrations2 <- left_join(registrations, teams)#4263 -va a faltar la persona nueva

length(setdiff(teams$attendee_email, registrations2$attendee_email))#no se han inscrito 252
length(setdiff(registrations2$attendee_email, teams$attendee_email))#117 se inscribieron sin deber
filter(registrations2, !attendee_email %in% teams$attendee_email) %>% 
  count(assigned_land_1)#499

registrations3 <- registrations2 %>%
  filter(!attendee_email %in% sai$attendee_email) %>% #4254 gente que sale por sai
  filter(!is.na(assigned_land_1)) #gente que sale por no tener land antes era 3773 pero es 3764

registration_attendees <- registrations2 %>% 
  select(ID, starts_with("attendee")) %>% 
  distinct() %>% 
  arrange(ID) #879-880

write_csv(registration_attendees, "data/07_registered_attendees.csv") #DONT PUSH

## Anonymize reg----
registration_nn <- registrations3 %>% 
  select(-starts_with("attendee"))
write_csv(registration_nn, "data/08_registrations.csv")#4254 -> 3764
#la gente que no esta en teams se quedó sin ID