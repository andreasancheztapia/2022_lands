# Decisiones sobre los conflictos de horario

- `9_new_waitlist.csv` entró al excel en la primera pestaña (hoy la pestaña se llama `10_a_mano.csv`)
- organicé dos tablas dinámicas para ver el horario de cada participante en orden cronológico y los conflictos
- creé una columna nueva que se llama `decision2`, y para cada par de conflictos dije quién se QUEDA y quien SALE. El resto de valores son anotaciones sobre sesiones que están llenas y la existencia de waitlists. Pero eso toca resolverlo después, cuando las nuevas filas queden. 
- Sólo edité `decision2` y sólo actualicé en las tablas dinámicas, que __no se tocan__
- Al terminar todos los conflictos, guardé la primera pestaña como `10_a_mano.csv` (por eso el nombre)
- `10_a_mano.csv` entra a R, se vuelven a calcular filas, conflictos y tiempo de cada persona, eso está en `R/04_check_again.R` y el output es `results/11_without_conflicts.csv` 